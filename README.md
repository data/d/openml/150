# OpenML dataset: covertype

https://www.openml.org/d/150

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Albert Bifet  
**Source**: [MOA](http://moa.cms.waikato.ac.nz/datasets/) - 2009  
**Please cite**:   

Normalized version of the Forest Covertype dataset (see version 1), so that the numerical values are between 0 and 1. Contains the forest cover type for 30 x 30 meter cells obtained from US Forest Service (USFS) Region 2 Resource Information System &#40;RIS&#41; data. It contains 581,012 instances and 54 attributes, and it has been used in several papers on data stream classification.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/150) of an [OpenML dataset](https://www.openml.org/d/150). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/150/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/150/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/150/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

